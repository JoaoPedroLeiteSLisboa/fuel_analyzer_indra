package br.com.indra.app.api.model;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelAdapter.class)
public abstract class ModelAdapter_ extends br.com.indra.app.api.model.ModelMappedSuperClass_ {

	public static volatile SingularAttribute<ModelAdapter, String> codigo;
	public static volatile SingularAttribute<ModelAdapter, String> siglaEstado;
	public static volatile SingularAttribute<ModelAdapter, String> produto;
	public static volatile SingularAttribute<ModelAdapter, String> municipio;
	public static volatile SingularAttribute<ModelAdapter, Double> valorVenda;
	public static volatile SingularAttribute<ModelAdapter, Double> valorCompra;
	public static volatile SingularAttribute<ModelAdapter, String> unidadeMedida;
	public static volatile SingularAttribute<ModelAdapter, LocalDate> dataColeta;
	public static volatile SingularAttribute<ModelAdapter, String> regiao;
	public static volatile SingularAttribute<ModelAdapter, String> revendaInstalacao;
	public static volatile SingularAttribute<ModelAdapter, String> bandeira;

	public static final String CODIGO = "codigo";
	public static final String SIGLA_ESTADO = "siglaEstado";
	public static final String PRODUTO = "produto";
	public static final String MUNICIPIO = "municipio";
	public static final String VALOR_VENDA = "valorVenda";
	public static final String VALOR_COMPRA = "valorCompra";
	public static final String UNIDADE_MEDIDA = "unidadeMedida";
	public static final String DATA_COLETA = "dataColeta";
	public static final String REGIAO = "regiao";
	public static final String REVENDA_INSTALACAO = "revendaInstalacao";
	public static final String BANDEIRA = "bandeira";

}

