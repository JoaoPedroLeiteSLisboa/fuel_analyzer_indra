package br.com.indra.app.api.exception;

public class TipoDeArquivoException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -255101952848916092L;

	public TipoDeArquivoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipoDeArquivoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TipoDeArquivoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TipoDeArquivoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TipoDeArquivoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
	
}
