package br.com.indra.app.api.resource;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.indra.app.api.service.StorageFileService;

@RestController
@RequestMapping("/import")
public class ImportFileResource {



	@Autowired
	private StorageFileService storeFileService;

	@RequestMapping(method = RequestMethod.POST, value = "/csv")
	public void uploudCsv(@RequestParam(name = "file") MultipartFile file)  {

		System.out.println("testeDebug");
		try {
			storeFileService.uploudServiceCsv(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



}
