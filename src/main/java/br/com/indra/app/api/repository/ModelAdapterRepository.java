package br.com.indra.app.api.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.indra.app.api.dto.ValorMedioVendaECompra;
import br.com.indra.app.api.model.ModelAdapter;
import br.com.indra.app.api.repository.modeladapter.ModelAdapterRepositoryQuery;

@Repository
public interface ModelAdapterRepository extends JpaRepository<ModelAdapter, Long>, ModelAdapterRepositoryQuery {

	@Query(value = "SELECT avg(ValorVenda) FROM ModelAdapter WHERE SiglaMunicipio = ?1", nativeQuery = true)
	Double findAvgValorVendaMunicipio(@Param("municipio") String municipio);

	@Query(value = "SELECT avg(ValorCompra) FROM ModelAdapter WHERE SiglaMunicipio = ?1", nativeQuery = true)
	Double findAvgValorCompraMunicipio(@Param("municipio") String municipio);
	
    @Query(value = "SELECT ma FROM ModelAdapter ma GROUP BY DataColeta")
    List<ModelAdapter> findAllGroupByDataColeta();
	
    @Query(value = "SELECT new br.com.indra.app.api.dto.ValorMedioVendaECompra(avg(ValorCompra) as valorMedioCompra, avg(ValorVenda) as valorMedioVenda) FROM ModelAdapter WHERE Bandeira = ?1", nativeQuery = true)
    ValorMedioVendaECompra findAvgValorBandeira(String bandeira);
	
	
	@Query(value = "select ma from ModelAdapter ma " + "where ma.dataColeta = :data ", nativeQuery = true)
	Collection<ModelAdapter> recuperarHistoricoPorData(@Param("data") LocalDate data);

	@Query(value = "SELECT trunc(SUM(ma.ValorCompra)/ COUNT(ma.*),2) " + "FROM ModelAdapter ma "
			+ "where  Bandeira = :bandeira;", nativeQuery = true)
	BigDecimal recuperarValorMedioCompraPorBandeira(@Param("bandeira") String bandeira);

	@Query(value = "SELECT trunc(SUM(ma.ValorVenda)/ COUNT(ma.*),2) " + "FROM ModelAdapter ma "
			+ "where  Bandeira = :bandeira;", nativeQuery = true)
	BigDecimal findAvgValorVendaECompraBandeira(@Param("bandeira") String bandeira);

}
