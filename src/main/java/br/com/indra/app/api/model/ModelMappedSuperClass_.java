package br.com.indra.app.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ModelMappedSuperClass.class)
public abstract class ModelMappedSuperClass_ {

	public static volatile SingularAttribute<ModelMappedSuperClass, Long> id;

	public static final String ID = "id";

}

