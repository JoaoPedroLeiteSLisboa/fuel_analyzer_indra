package br.com.indra.app.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuelAnalyzerIndraApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuelAnalyzerIndraApplication.class, args);
	}

}
