package br.com.indra.app.api.repository.modeladapter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.indra.app.api.model.ModelAdapter;
import br.com.indra.app.api.repository.filter.ModelAdapterFilter;



public interface ModelAdapterRepositoryQuery {
	
	public Page<ModelAdapter> filtrar(ModelAdapterFilter filter, Pageable pageable);

}
