package br.com.indra.app.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.app.api.dto.ValorMedioVendaECompra;
import br.com.indra.app.api.event.RecursoCriadorEvent;
import br.com.indra.app.api.model.ModelAdapter;
import br.com.indra.app.api.repository.ModelAdapterRepository;
import br.com.indra.app.api.repository.filter.ModelAdapterFilter;
import br.com.indra.app.api.service.ServiceModelAdapter;

@RestController
@RequestMapping("/2018-1_CA")
public class ModelAdapterResource {

	@Autowired
	private ModelAdapterRepository repository;
	
	@Autowired ServiceModelAdapter service;
	
	@Autowired
	private ApplicationEventPublisher publisherEvent;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Page<ModelAdapter> pesquisar(ModelAdapterFilter filter, Pageable page) {
		return repository.filtrar(filter, page);
	}
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ModelAdapter> create(@Valid @RequestBody ModelAdapter modelAdapter, HttpServletResponse response) {
		ModelAdapter modelAdapterSalvo = service.save(modelAdapter);
		publisherEvent.publishEvent(new RecursoCriadorEvent(this, response, modelAdapterSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(modelAdapterSalvo);
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@RequestMapping(method = RequestMethod.GET, value = "/id/{id}")
	public ResponseEntity<ModelAdapter> findById(@PathVariable(required = true) Long id){
		
		return ResponseEntity.ok(service.findByIdModelAdapter(id).get());
	}

	@RequestMapping(method = RequestMethod.GET, value = "/find/media/valor/compra/municipio", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Double findAvgValorCompra(@RequestParam(name = "municipio", required = true) String municipio) {
		return repository.findAvgValorCompraMunicipio(municipio);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/find/media/valor/venda/municipio", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Double findAvgValorVenda(@RequestParam(name = "municipio", required = true) String municipio) {
		return repository.findAvgValorVendaMunicipio(municipio);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/find/media/valor/compraevenda/bandeira", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ValorMedioVendaECompra valorMedioVendaECompra(
			@RequestParam(name = "bandeira", required = true) String bandeira) {
		return repository.findAvgValorBandeira(bandeira);
	}

	// Necessita Reparo....
	@GetMapping(value = "/find/all/groupdatacoleta", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<ModelAdapter> findAllGroupByDataColeta() {
		return repository.findAllGroupByDataColeta();
	}

}
