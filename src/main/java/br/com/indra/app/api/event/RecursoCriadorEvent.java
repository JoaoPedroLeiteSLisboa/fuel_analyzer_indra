package br.com.indra.app.api.event;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecursoCriadorEvent extends ApplicationEvent {

	/**
	 * 
	 */

	public RecursoCriadorEvent(Object source, HttpServletResponse response, Long codigo) {
		super(source);
		this.response = response;
		this.codigo = codigo;
	}

	private static final long serialVersionUID = -8402168778758358697L;
	private HttpServletResponse response;
	private Long codigo;

}
