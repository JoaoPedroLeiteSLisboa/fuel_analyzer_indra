package br.com.indra.app.api.exception;

public class CpfInvalidoOuInexistenteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4539715679356727983L;

	public CpfInvalidoOuInexistenteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	
}
