package br.com.indra.app.api.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import br.com.indra.app.api.exception.TipoDeArquivoException;
import br.com.indra.app.api.model.ModelAdapter;
import br.com.indra.app.api.repository.ModelAdapterRepository;
import br.com.indra.app.api.utils.TypeFilesUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class StorageFileService {

    private final ModelAdapterRepository modelAdapterRepository;
    private String path;
    private String name;
    private String type;
    private Long size;

    public void uploudServiceCsv(MultipartFile file) throws IOException {

        path = "";
        name = file.getOriginalFilename();
        type = file.getContentType();
        size = file.getSize();

        //VALIDANDO TIPO DE ARQUIVO SELECIONADO
        isValidoTipoArquivo(type);

        byte[] bytes = file.getBytes();

        // Creating the directory to store file
        path = createPath();

        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdirs();
        }


        // Create the file on server
        File serverFile = new File(dir.getAbsolutePath() + "\\" + name);

        if (!serverFile.exists()) {

            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));

            stream.write(bytes);

            stream.close();
            System.out.println(path);
            try {
                readFileCsvToBeanAndPersist(serverFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Transactional
    public void readFileCsvToBeanAndPersist(String path) throws IOException {
        log.info("Iniciando Persistencia do  arquivo no Banco de dados");

        final Double valorZerado = 0.0;

        System.out.println(path);
        Reader reader = Files.newBufferedReader(Paths.get(path));

        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

        List<String[]> modelAdapterList = csvReader.readAll();

        for (String[] modelAdapterArray : modelAdapterList) {

            LocalDate data = LocalDate.parse(modelAdapterArray[6], DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            System.out.println(data);

            double valorVenda = valorZerado;
            double valorCompra = valorZerado;

            if (!modelAdapterArray[8].isEmpty()) {
                valorVenda = Double.parseDouble(modelAdapterArray[8]);
            }
            if (!modelAdapterArray[7].isEmpty()) {
                valorCompra = Double.parseDouble(modelAdapterArray[7]);
            }

            ModelAdapter toModelAdapter = new ModelAdapter(modelAdapterArray[0], modelAdapterArray[1], modelAdapterArray[2],
                    modelAdapterArray[3], modelAdapterArray[4], modelAdapterArray[5], data, valorCompra, valorVenda, modelAdapterArray[9],
                    modelAdapterArray[10]);

            modelAdapterRepository.save(toModelAdapter);
            log.info("Arquivo persistido no Banco de dados");
            log.info("File Path: "+path);

        }

    }


    private void isValidoTipoArquivo(String type) {

        if (!type.equalsIgnoreCase(TypeFilesUtils.MIME_TEXT_X_CSV)) {
            throw new TipoDeArquivoException("Tipo de arquivo invalido: O mesmo deve possuir Extenção: "+ TypeFilesUtils.MIME_TEXT_X_CSV);

        }
    }

    private String createPath() {


        String data = String.valueOf(LocalDate.now());
        String hora = String.valueOf(LocalTime.now());

        return "C:\\Users\\Pedro\\Documents\\dados" + "\\" + data + "\\" + hora.replaceAll("[:.]", "");
    }

}




