package br.com.indra.app.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.indra.app.api.model.ModelAdapter;
import br.com.indra.app.api.repository.ModelAdapterRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class ServiceModelAdapter {

	private final ModelAdapterRepository modelAdapterRepository;

	@Transactional
	public ModelAdapter save(ModelAdapter modelAdapter) {

		log.info("Cadastrando um Novo Lançamento de Preços de Combustives Brasileiros: " + modelAdapter.toString());

		return modelAdapterRepository.save(modelAdapter);

	}
	
	public Optional<ModelAdapter> findByIdModelAdapter(Long id) {

		log.info("Find ModelAdapter id: " + id);
		Optional<ModelAdapter> modelAdapterSalvo = modelAdapterRepository.findById(id);
		if (!modelAdapterSalvo.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return modelAdapterSalvo;
	}

}
