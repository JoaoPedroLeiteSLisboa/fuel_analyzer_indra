package br.com.indra.app.api.repository.modeladapter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.indra.app.api.model.ModelAdapter;
import br.com.indra.app.api.model.ModelAdapter_;
import br.com.indra.app.api.repository.filter.ModelAdapterFilter;

public class ModelAdapterRepositoryImpl implements ModelAdapterRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<ModelAdapter> filtrar(ModelAdapterFilter ModelAdapterFilter, Pageable pageable) {
		// TODO Auto-generated method stub

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ModelAdapter> criteria = builder.createQuery(ModelAdapter.class);
		Root<ModelAdapter> root = criteria.from(ModelAdapter.class);

		Predicate[] predicates = criarRestricoes(ModelAdapterFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<ModelAdapter> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(ModelAdapterFilter));
	}

	private Predicate[] criarRestricoes(ModelAdapterFilter ModelAdapterFilter, CriteriaBuilder builder,
			Root<ModelAdapter> root) {
		List<Predicate> predicates = new ArrayList<>();

		// Filtro por Municipio: Listando por Municipio
		if (!StringUtils.isEmpty(ModelAdapterFilter.getMunicipio())) {
			predicates.add(builder.like(builder.lower(root.get(ModelAdapter_.MUNICIPIO)),
					"%" + ModelAdapterFilter.getMunicipio().toLowerCase() + "%"));
		}

		// Filtro por Sigla - Estado: Listando por sigla de Estado

		if (!StringUtils.isEmpty(ModelAdapterFilter.getSiglaEstado())) {
			predicates.add(builder.like(builder.lower(root.get(ModelAdapter_.SIGLA_ESTADO)),
					"%" + ModelAdapterFilter.getSiglaEstado().toLowerCase() + "%"));
		}

		// Filtro por Região: Listando por região brasileira
		if (!StringUtils.isEmpty(ModelAdapterFilter.getRegiao())) {
			predicates.add(builder.like(builder.lower(root.get(ModelAdapter_.REGIAO)),
					"%" + ModelAdapterFilter.getRegiao().toLowerCase() + "%"));
		}

		// Filtro por Bandeira: Listando por Bandeira da Empresa Distribuidora
		if (!StringUtils.isEmpty(ModelAdapterFilter.getBandeira())) {
			predicates.add(builder.like(builder.lower(root.get(ModelAdapter_.BANDEIRA)),
					"%" + ModelAdapterFilter.getBandeira().toLowerCase() + "%"));
		}

		// Filtro por Empresa: Listando por Empresa
		if (!StringUtils.isEmpty(ModelAdapterFilter.getRevendaInstalacao())) {
			predicates.add(builder.like(builder.lower(root.get(ModelAdapter_.REVENDA_INSTALACAO)),
					"%" + ModelAdapterFilter.getRevendaInstalacao().toLowerCase() + "%"));
		}

		// Filtro por Tipo de Combustivel: Listando por Tipo de Combustivel
		if (!StringUtils.isEmpty(ModelAdapterFilter.getProduto())) {
			predicates.add(builder.like(builder.lower(root.get(ModelAdapter_.PRODUTO)),
					"%" + ModelAdapterFilter.getProduto().toLowerCase() + "%"));
		}

		// Filtro por data de Coleta_De: Listando por Data de Coleta de determinado
		// Inicio
		if (ModelAdapterFilter.getDataColetaDe() != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get(ModelAdapter_.DATA_COLETA),
					ModelAdapterFilter.getDataColetaDe()));
		}
		// Filtro por data de Coleta_Ate: Listando por Data de Coleta de ate determinado
		// dia
		if (ModelAdapterFilter.getDataColetaAte() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get(ModelAdapter_.DATA_COLETA),
					ModelAdapterFilter.getDataColetaAte()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}

	private Long total(ModelAdapterFilter ModelAdapterFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ModelAdapter> root = criteria.from(ModelAdapter.class);

		Predicate[] predicates = criarRestricoes(ModelAdapterFilter, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
