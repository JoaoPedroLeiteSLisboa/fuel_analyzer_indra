
### Fuel Analyzer Indra Company
Simplista exemplo de um sistema para apresentação, filtragem e coleta de dados da Agência nacional de petroleo e gas (2018).

### Sobre o Projeto 

O supracitado Projeto tem como objetivo geral, realizar filtragem de Registro de Lançamentos de Combustives de Postos Brasileiros 


### Apresentação Técnica.

Escrito seguindo implementações Java e  SpringBoot 
    
### Tecnologias e Recursos utilizados - Back-End.
    
    * Java 1.8^^
    
    * SpringBoot 2.1.7
 
   
    
 	* Hibernate / Hibernate-jpamodelgen
    
    * jackson-datatype-jsr310 / jackson-databind 

    * apache.commons

    * E entre outros Recursos para funcionamento e disposição da Regra de Negocio.
    
    

### Tecnologias e Recursos utilizados - Front-End.
    
		Em desenvolvimento...


### Instruções para funcionamento - 


	* Este Projeto possui dois perfis de Execução (application-dev e application-production), 
	Contudo o application-production vem apresentando melhor eficiência no quesito consultas mais aprimoradas devido certas incompatibilidades do h2database...
	
	Lembre-se de adicionar o nome de seu usuario e senha do Banco de dados.
	
	
### Instruções para uso -	Segue alguns, de varios paths para requsitação...  

	 
	OBS: O arquivo para Upload necessita ser o que estar presente na pasta /src/main/resources/static (Devido melhor compatibilidade com o codigo)
		
	* Upload do Arquivo para Leitura e Persistencia na Base de dados
	
	RequestMethod.POST >>> Key: file >>> Value Type: File    http://localhost:8080/api/import/csv
	
	
	 O mesmo será persistido em um local personalizado no S.O Windows... Conforme breve exemplo asseguir>>>  "C:\\Users\\Pedro\\Documents\\dados" + "\\" + data + "\\" + hora.replaceAll("[:.]", "")
	
	* Listando todos os registros da Base de dados
	
	RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA/
	
	
	
	* Buscando registros por Id (do banco de dados)
	
	RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA/id/{value}
	
	
	
	
	* Buscando registros por Id (do banco de dados)
	
	RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA/id/{value}
		



	* Retorna uma determinada Bandeira (Empresa) com data de coleta inicial ate uma outra determinada data fim
	 
	RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA?bandeira=BRANCA&dataColetaDe=2018-01-01&dataColetaAte=2018-01-10
	



	* Retorna Dados (Lançamentos) por Municipio
    
    RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA?municipio=BRASILIA




	* Media de Compra Por um determinado Municipio	
	  
	RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA/find/media/valor/compra/municipio?municipio=GOIATUBA  
	
	
	
	
	* Media de Venda Por um determinado Municipio	
	  
	RequestMethod.GET >>> http://localhost:8080/api/2018-1_CA/find/media/valor/venda/municipio?municipio=BRASILIA  
	
	
	
	
	* Salvando um Lançamento de Preços na Base de dados
	RequestMethod.POST >>> Type: application/json >>> 
	
	Body >>>    {
           
            "regiao": "CO",
            "siglaEstado": "PB",
            "municipio": "JOAOPESSOA",
            "revendaInstalacao": "AUTO POSTO INDRA COMPANY",
            "codigo": "3333",
            "produto": "GASOLINA",
            "dataColeta": "10-08-2019",
            "valorCompra": 3.699,
            "valorVenda": 4.12,
            "unidadeMedida": "R$ / litro",
            "bandeira": "Salvando um Novo Lançamento de um determinada Empresa"
        }
	
	
	
	
	  
### Autor

Nome: João Pedro Leite S Lisboa.

Linkedin: https://www.linkedin.com/in/joaopedroleiteslisboa/

GitHub: https://github.com/joaopedroleiteslisboa




